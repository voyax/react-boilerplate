import "babel-polyfill";
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Todo from 'Containers/Todo/index';

ReactDOM.render(<Todo />, document.getElementById('root'));