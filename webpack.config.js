const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
module.exports = {
  devtool: 'eval-source-map',
  entry: __dirname + '/app/index.js',
  output: {
    path: __dirname + '/build',
    filename: 'bundle-[hash].js',
  },

  devServer: {
    contentBase: './public',
    historyApiFallback: true,
    inline: true,
  },

  module: {
    rules: [
      {
        test: /(\.jsx|\.js)$/,
        use: {
          loader: 'babel-loader',
        },
        exclude: /node_modules/,
      },
      {
        test: /\.less$/,
        use: [{
          loader: 'style-loader',
        },{
          loader: 'css-loader',
          options: {
            // modules: true,
          },
        },{
          loader: 'postcss-loader',
        },
        {
          loader: "less-loader" // compiles Less to CSS
        }]
      }
    ]
  },

  plugins: [
    new webpack.BannerPlugin('Author: voya; Email: voyax3@gmail.com'),
    new HtmlWebpackPlugin({
      template: __dirname + '/app/index.tmpl.html'
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],

  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      Containers: path.resolve(__dirname, 'app/containers/'),
      Components$: path.resolve(__dirname, 'app/components/')
    },
  },
}